import java.sql.*;
import java.util.*;
public class Login {
    public static void main(String[] args) throws SQLException {
        // write your code here
        Scanner scan=new Scanner(System.in);
        boolean start=true;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Accounts", "root", "root");
            while (start) {
                System.out.println("Please choose which you want to perform:\n 1)Sign Up\n 2)Login\n 3)Account Details\n 4)Edit\n 5)Delete\n 6)Exit");
                int op = scan.nextInt();
                switch (op) {
                    case 1:
                        insertData(con, scan);
                        break;
                    case 2:
                        Login(con, scan);
                        break;
                    case 3:
                        fetchData(con, scan);
                        break;
                    case 4:
                        UpdateData(con, scan);
                        break;
                    case 5:
                        deleteData(con, scan);
                        break;
                    default:
                        start=false;
                        System.out.println("Thank you for using our service");
                        con.close();
                }
             }
            }
            catch(SQLException | ClassNotFoundException SE){
                System.out.println(SE);
            }
        }

        public static void fetchData(Connection con,Scanner scan) throws SQLException {
            PreparedStatement stmt=con.prepareStatement("select * from PersonDetails  where Email_id = ?");
            System.out.print("Please enter your mail: ");
            scan.nextLine();
            stmt.setString(1,scan.nextLine());
            ResultSet rs=stmt.executeQuery();
            while(rs.next()) {
                System.out.println("Name: "+rs.getString(2)+"\nEmail: "+rs.getString(3)+"\nMobile:"+rs.getString(4));
            }
            rs=stmt.executeQuery();
            if(rs.next()==false) {
                System.out.println("Email id is invalid \n Please Sign Up");
            }

        }
        public static void insertData(Connection con,Scanner scan) throws SQLException {

         PreparedStatement stmt=con.prepareStatement("insert into PersonDetails(Name,Email_id,Mobile_Number,Password) values(?,?,?,?)");
         System.out.print("Please enter the name: ");
         scan.nextLine();
         stmt.setString(1,scan.nextLine());
         System.out.print("Please enter the mail: ");
         stmt.setString(2,scan.nextLine());
         System.out.print("Please enter the mobile number: ");
         stmt.setString(3,scan.nextLine());
         System.out.print("Please enter the password: ");
         stmt.setString(4,scan.nextLine());
         stmt.executeUpdate();
        System.out.println("Updated Successfully");
    }
    public static void deleteData(Connection con,Scanner scan) throws SQLException {
        PreparedStatement stmt=con.prepareStatement("delete from PersonDetails where Mobile_Number=?");
        System.out.print("Please enter your Mobile number you want to delete :");
        scan.nextLine();
        String Mnumber=scan.nextLine();
        stmt.setString(1,Mnumber);
        System.out.print("Are you sure you want to delete? [Y,N] ");
        String opti= scan.nextLine();
        if(opti.charAt(0)=='Y' || opti.charAt(0)=='y') {
             int rs= stmt.executeUpdate();
             if(rs==0) {
                 System.out.println("Your Mobile number is invalid");
             }
             else {
                 System.out.println("Your data is successfully deleted");
             }
        }
       }
    public static void UpdateData(Connection con,Scanner scan) throws SQLException {
        boolean flag=true;
        System.out.print("Please enter the mobile number: ");
        scan.nextLine();
        String number=scan.nextLine();
        while(flag) {
         System.out.println("Which you want to update:\n 1)Name\n 2)Email id\n 3)Mobile Number\n 4)Password\n 5)Exit");
         int option=scan.nextInt();
         scan.nextLine();
             switch (option) {
                 case 1:
                     PreparedStatement stmt = con.prepareStatement("update PersonDetails set Name=? where Mobile_Number=?");
                     stmt.setString(2, number);
                     System.out.print("Please enter the name: ");
                     stmt.setString(1, scan.nextLine());
                     int rs = stmt.executeUpdate();
                     if (rs == 0) {
                         System.out.println("Failed, Your Input is wrong.Try Again");
                     } else {
                         System.out.println("Your name updated Successfully");
                         System.out.print("Do you want to continue[Y/N]?: ");
                         String opt= scan.nextLine();
                         if(opt.charAt(0)=='N' || opt.charAt(0)=='n') { flag=false; }
                     }
                     break;
                 case 2:
                     PreparedStatement stmt1 = con.prepareStatement("update PersonDetails set Email_id=? where Mobile_Number=?");
                     stmt1.setString(2, number);
                     System.out.print("Please enter the new Email id: ");
                     stmt1.setString(1, scan.nextLine());
                     int rs1 = stmt1.executeUpdate();
                     if (rs1 == 0) {
                         System.out.println("Failed, Your Input is wrong.Try Again");
                     } else {
                         System.out.println("Your Email id updated Successfully");
                         System.out.print("Do you want to continue[Y/N]?: ");
                         String opt= scan.nextLine();
                         if(opt.charAt(0)=='N' || opt.charAt(0)=='n') { flag=false; }
                     }
                     break;
                 case 3:
                     PreparedStatement stmt2 = con.prepareStatement("update PersonDetails set Mobile_Number=? where Mobile_Number=?");
                     stmt2.setString(2, number);
                     System.out.print("Please enter the new mobile number: ");
                     stmt2.setString(1, scan.nextLine());
                     int rs2 = stmt2.executeUpdate();
                     if (rs2 == 0) {
                         System.out.println("Failed, Your Input is wrong.Try Again");
                     } else {
                         System.out.println("Your mobile number updated Successfully");
                         System.out.print("Do you want to continue[Y/N]?: ");
                         String opt= scan.nextLine();
                         if(opt.charAt(0)=='N' || opt.charAt(0)=='n') { flag=false; }
                     }
                     break;
                 case 4:
                     PreparedStatement stmt3 = con.prepareStatement("update PersonDetails set Password=? where Mobile_Number=? and Password=?");
                     System.out.println("Please enter your old password: ");
                     stmt3.setString(3, scan.nextLine());
                     stmt3.setString(2, number);
                     System.out.print("Please enter the new password: ");
                     stmt3.setString(1, scan.nextLine());
                     int rs3 = stmt3.executeUpdate();
                     if (rs3 == 0) {
                         System.out.println("Failed, Your Input is wrong.Try Again");
                     } else {
                         System.out.println("Your Password updated Successfully");
                         System.out.print("Do you want to continue[Y/N]?: ");
                         String opt= scan.nextLine();
                         if(opt.charAt(0)=='N' || opt.charAt(0)=='n') { flag=false; }
                     }
                     break;
                 default:
                     System.out.println("Exiting from edit option");
                     flag=false;
                     break;
             }
         }
    }
      public static void Login(Connection con,Scanner scan) throws SQLException {
          System.out.print("Please enter your Mail: ");
          scan.nextLine();
          String email= scan.nextLine();
          System.out.print("Please enter your Password: ");
          String pass= scan.nextLine();
          PreparedStatement stmt=con.prepareStatement("select * from PersonDetails  where Email_id = ? and Password=?");
          stmt.setString(1,email);
          stmt.setString(2,pass);
          ResultSet rs= stmt.executeQuery();
          if(rs.next()==false) {
              System.out.println("Your email or password is incorrect");
          }
          else {
              System.out.println("Hi "+rs.getString(2)+" You are LoggedIn successfully");
          }
      }
}
