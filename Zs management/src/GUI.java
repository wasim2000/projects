import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
public class GUI {
    private JTextField txtname;
    private JTextField txtid;
    private JTextField txtloc;
    private JButton button1;
    private JButton updateButton;
    private JButton deleteButton;
    private JButton clearButton;
    private JPanel Main;

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI");
        frame.setContentPane(new GUI().Main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    Connection con;
    PreparedStatement stmt;
    public void connect()  {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ZSTTK", "root", "root");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public GUI() {
        connect();
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name=txtname.getText();
                String id=txtid.getText();
                String loc=txtloc.getText();
                try {
                    stmt = con.prepareStatement("insert into Students values(?,?,?)");
                    stmt.setString(1,id);
                    stmt.setString(2,name);
                    stmt.setString(3,loc);
                    int rs= stmt.executeUpdate();
                    if(rs>0) {
                        JOptionPane.showMessageDialog(null,"Your Data Added Successfully");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"Failed to Add Data");
                    }
                    txtname.setText("");
                    txtid.setText("");
                    txtloc.setText("");
                } catch(Exception s) {
                    s.printStackTrace();
                }
            }
        });
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name=txtname.getText();
                String id=txtid.getText();
                String loc=txtloc.getText();
                try {
                    stmt = con.prepareStatement("update Students set Name=?, Location=? where Zsid=?");
                    stmt.setString(1,name);
                    stmt.setString(2,loc);
                    stmt.setString(3,id);
                    int rs= stmt.executeUpdate();
                    if(rs>0) {
                        JOptionPane.showMessageDialog(null,"Your value is updated");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"Your value is failed to update");
                    }
                    txtname.setText("");
                    txtid.setText("");
                    txtloc.setText("");
                } catch (Exception a) {
                    a.printStackTrace();
                }
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id=txtid.getText();
                try{
                    stmt = con.prepareStatement("delete from Students where Zsid=?");
                    stmt.setString(1,id);
                    int rs= stmt.executeUpdate();
                    if(rs>0) {
                        JOptionPane.showMessageDialog(null,"Your value is deleted");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"Your value is failed to delete");
                    }
                    txtname.setText("");
                    txtid.setText("");
                    txtloc.setText("");
                }catch (Exception b) {
                    b.printStackTrace();
                }
            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtname.setText("");
                txtid.setText("");
                txtloc.setText("");
            }
        });

        };
    }

