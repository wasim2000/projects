import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StudentMarksCalculation {
    private JTextField txtname;
    private JTextField txtmark1;
    private JTextField txtmark2;
    private JTextField txtmark3;
    private JTextField txtTotal;
    private JTextField txtaverage;
    private JButton proceedButton;
    private JTextField txtResult;
    private JPanel Main;

    public static void main(String[] args) {
        JFrame frame = new JFrame("StudentMarksCalculation");
        frame.setContentPane(new StudentMarksCalculation().Main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public StudentMarksCalculation() {
        proceedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                       int m1,m2,m3,total;
                       double average;
                       m1=Integer.parseInt(txtmark1.getText());
                       m2=Integer.parseInt(txtmark2.getText());
                       m3=Integer.parseInt(txtmark3.getText());
                       total=m1+m2+m3;
                       txtTotal.setText(String.valueOf(total));
                       average=total/3;
                       txtaverage.setText(String.valueOf(average));
                       if (m1<35 || m2<35 || m3<35)   {
                           txtResult.setText("Failed");
                       }
                       else {
                           txtResult.setText("Passed");
                       }
            }
        });
    }
}
