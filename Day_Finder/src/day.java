import java.util.Scanner;

public class day {
    public static void main(String[] args) {
        int Year,Month,Day,Remain_Day;
        int Odd_Days=0;
        String [] Days=new String[]{"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
        Scanner scan=new Scanner(System.in);
        do {
            System.out.println("Enter the year");
            Year=scan.nextInt();
        } while (Year<0);

        do {
            System.out.println("Enter the month in proper number (eg: 1 for January,2 for February, so on...");
            Month=scan.nextInt();
        } while (Month<=0 || Month>12);

        do{
            System.out.println("Enter the day");
            Day=scan.nextInt();
            while (Month==1  &&  Day>31) {
                System.out.println("Please enter the proper value of January");
                Day=scan.nextInt();
            }
            while( (Month==2 && Day>28 && (Year%400!=0 && Year%100==0) ) || (Month==2 && Day>28 && Year%4!=0) || (Month==2 && Day>29 && Year%4==0) ) {
                System.out.println("Please enter the proper day of February");
                Day=scan.nextInt();
            }
            while (Month==3  &&  Day>31) {
                System.out.println("Please enter the proper value of March");
                Day=scan.nextInt();
            }
            while (Month==4  &&  Day>30) {
                System.out.println("Please enter the proper value of April");
                Day=scan.nextInt();
            }
            while (Month==5  &&  Day>31) {
                System.out.println("Please enter the proper value of May");
                Day=scan.nextInt();
            }
            while (Month==6  &&  Day>30) {
                System.out.println("Please enter the proper value of June");
                Day=scan.nextInt();
            }
            while (Month==7  &&  Day>31) {
                System.out.println("Please enter the proper value of July");
                Day=scan.nextInt();
            }
            while (Month==8  &&  Day>31) {
                System.out.println("Please enter the proper value of August");
                Day=scan.nextInt();
            }
            while ( (Month==9 && Year==1752) && (Day>2 && Day<14) ) {

                System.out.println(" Great Britain and the British Empire use the Julian calendar for the last time and adopt the Gregorian calendar, making the next day Thursday, September 14 from September 2 in the English-speaking world.");
                System.out.println(" So please enter the date of september 1752, other than 3 to 13 ");
                Day=scan.nextInt();
            }

            while (Month==9  &&  Day>30) {
                System.out.println("Please enter the proper value of September");
                Day=scan.nextInt();
            }
            while (Month==10  &&  Day>31) {
                System.out.println("Please enter the proper value of October");
                Day=scan.nextInt();
            }
            while (Month==11  &&  Day>30) {
                System.out.println("Please enter the proper value of November");
                Day=scan.nextInt();
            }
            while (Month==12  &&  Day>31) {
                System.out.println("Please enter the proper value of December");
                Day=scan.nextInt();
            }

        } while (Day<=0 || Day>31);

        Remain_Day=(Year-1)%400;
        if ( Remain_Day>=300)  {
            Odd_Days=1;     }
        else if (Remain_Day>=200) {
            Odd_Days=3;  }
        else if (Remain_Day>=100) {
            Odd_Days=5;   }

        int Remain_Years=Remain_Day%100;
        int leapyear=Remain_Years/4;
        Odd_Days+=2*leapyear;
        int non_leapyear= Remain_Years-leapyear;
        Odd_Days+=non_leapyear;

        int[] Month_Odd=new int[]{3,0,3,2,3,2,3,3,2,3,2,3};
        for (int count=0;count<(Month-1);count++) {
            Odd_Days+=Month_Odd[count];
        }
        if (Month>2) {
            if (((Year%4==0) && (Year%100!=0)) || (Year%400==0)) {
                Odd_Days+=1;
            }
        }


        Odd_Days+=Day;
        Odd_Days=Odd_Days%7;


        System.out.println("The day is " + Days[Odd_Days]);
        //System.out.println("THANK YOU, NOT SEE YOU AGAIN, BYE");
    }
}
