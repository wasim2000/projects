import java.lang.*;
import java.util.Scanner;





class Main {
    public static void main(String[] args) {
        ReverseString rev=new ReverseString();
        Scanner scan=new Scanner(System.in);
        System.out.println("Enter the function you want to perform in numerical(like 1,2....12):");
        System.out.println(String.format("%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s","1-Decimal to Binary", "2-Decimal to Octal", "3-Decimal to Hexadecimal","4-Binary to Decimal","5-Octal to Decimal","6-Hexadecimal to Decimal","7-Binary to Octal","8-Binary to HexaDecimal","9-Octal to Binary","10-HexaDecimal to Binary","11-Octal to HexaDecimal","12-HexaDecimal to Octal"));
        int choice=scan.nextInt();
        while(choice<1 || choice>12)  {
            System.out.println("Please choose options from 1 to 12");
            choice=scan.nextInt();
        }

        String b="";
        String rev_String="";
        switch(choice)  {
            case 1:
                System.out.print("You have chosen Decimal to Binary, Please Enter the Decimal number:");
                int decimal=scan.nextInt();
                while (decimal>1) {
                    int a=decimal%2;

                    b+=Integer.toString(a);
                    decimal=decimal/2;
                    if (decimal==1) {
                        a=1;
                        b+=Integer.toString(a);
                    }
                }

                rev_String=rev.reverse(b);
                System.out.println("The answer is "+rev_String);





                break;

            case 2:
                System.out.print("You have chosen Decimal to Octal, Please Enter the Decimal number:");
                decimal=scan.nextInt();

                while (decimal>7) {
                    int a=decimal%8;
                    b+=Integer.toString(a);
                    decimal=decimal/8;
                    if (decimal<=7) {
                        a=decimal;
                        b+=Integer.toString(a);
                    }
                }
                rev_String=rev.reverse(b);
                System.out.println("The answer is "+rev_String);
                break;

            case 3:
                System.out.print("You have chosen Decimal to Hexadecimal, Please Enter the Decimal number:");
                decimal=scan.nextInt();
                String[] remainder={"A","B","C","D","E","F"};
                while (decimal>15) {
                    int a=decimal%16;
                    decimal=decimal/16;
                    if(a>9)  {
                        a-=10;
                        b+=remainder[a];
                    }
                    else {
                        b+=Integer.toString(a);
                    }
                    if (decimal<=15) {
                        if(decimal>9) {
                            decimal-=10;
                            b+=remainder[decimal];
                        }
                        else {
                            a=decimal;
                            b+=Integer.toString(a);
                        }

                    }
                }
                rev_String=rev.reverse(b);
                System.out.println("The answer is "+rev_String);
                break;

            case 4:
                System.out.println("You have chosen Binary to Decimal,Please Enter the Binary Number");
                String binary=scan.next();
                int len_binary=binary.length();
                try {
                    for(int counted=0;counted<=len_binary;counted++)  {
                        char token=binary.charAt(counted);
                        int check=Integer.parseInt(String.valueOf(token));
                        while(check<0 || check>1)  {
                            System.out.println("Please Check your Binary number and try again");
                            binary=scan.next();
                            len_binary=binary.length();
                            counted=-1;
                            break;
                        }
                    }
                }   catch (StringIndexOutOfBoundsException e)  {

                }
                double answer=0;
                int temp_binary=len_binary-1;
                for (int i=0;i<len_binary;i++)  {
                    char ch=binary.charAt(i);
                    int value2=Integer.parseInt(String.valueOf(ch));
                    answer+=(value2) * (Math.pow(2,temp_binary));
                    temp_binary--;

                }
                System.out.println("The answer is "+answer);
                break;

            case 5:
                System.out.println("You have chosen Octal to Decimal,Please Enter the Octal Number");
                String octal=scan.next();
                int len_octal=octal.length();
                try {
                    for(int counted=0;counted<=len_octal;counted++)  {
                        char token=octal.charAt(counted);
                        int check=Integer.parseInt(String.valueOf(token));
                        while(check<0 || check>7)  {
                            System.out.println("Please Check your Octal number and try again");
                            octal=scan.next();
                            len_octal=octal.length();
                            counted=-1;
                            break;
                        }
                    }
                }   catch (StringIndexOutOfBoundsException e)  {

                }

                answer=0;
                int temp_octal=len_octal-1;
                for (int i=0;i<len_octal;i++)  {

                    char ch=octal.charAt(i);
                    int value2=Integer.parseInt(String.valueOf(ch));
                    answer+=(value2) * (Math.pow(8,temp_octal));
                    temp_octal--;

                }
                System.out.println("The answer is "+answer);
                break;

            case 6:
                System.out.println("You have chosen Hexadecimal to Decimal,Please Enter the Hexadecimal Number");
                String hexadecimal=scan.next();
                int len_hexa=hexadecimal.length();
                try  {
                    for(int counted=0;counted<=len_hexa;counted++)  {
                        char token=hexadecimal.charAt(counted);
                        int ascii=token;
                        while ((ascii>70 && ascii<96) || ascii>102)  {
                            System.out.println("Please Check your Hexadecimal number and try again");
                            hexadecimal=scan.next();
                            len_hexa=hexadecimal.length();
                            counted=-1;
                            break;
                        }

                    }

                }  catch (NumberFormatException e)   {

                }
                catch (Exception e)  {

                }




                answer=0;
                int temp_hexa=len_hexa-1;
                for (int i=0;i<len_hexa;i++)  {
                    int value3=0;
                    char ch=hexadecimal.charAt(i);
                    if(ch=='A' || ch=='a')   {
                        value3=10;
                    }
                    else if(ch=='B' || ch=='b')   {
                        value3=11;
                    }
                    else if(ch=='C' || ch=='c')   {
                        value3=12;
                    }
                    else if(ch=='D' || ch=='d')   {
                        value3=13;
                    }
                    else if(ch=='E' || ch=='e')   {
                        value3=14;
                    }
                    else if(ch=='F' || ch=='f')   {
                        value3=15;
                    }
                    else  {
                        value3=Integer.parseInt(String.valueOf(ch));
                    }

                    answer+=(value3) * (Math.pow(16,temp_hexa));
                    temp_hexa--;

                }
                System.out.println("The answer is "+answer);
                break;

            case 7:
                System.out.println("You have chosen Binary to Octal,Please Enter the Binary Number");


                String binary2=scan.next();
                len_binary=binary2.length();
                try {
                    for(int counted=0;counted<=len_binary;counted++)  {
                        char token=binary2.charAt(counted);
                        int check=Integer.parseInt(String.valueOf(token));
                        while(check<0 || check>1)  {
                            System.out.println("Please Check your Binary number and try again");
                            binary2=scan.next();
                            len_binary=binary2.length();
                            counted=-1;
                            break;
                        }
                    }
                }   catch (StringIndexOutOfBoundsException e)  {

                }
                String[] octal_list={"0","1","2","3","4","5","6","7"};
                int numerical=Integer.parseInt(binary2);
                int current_value=0;
                String answer2="";
                int digit3=0;
                while(binary2.length()>=3)   {
                    digit3=numerical%1000;
                    if(digit3==000 || digit3==0){
                        current_value=0;
                        answer2+=octal_list[current_value];
                    }   else if(digit3==001 || digit3==1)   {
                        current_value=1;
                        answer2+=octal_list[current_value];
                    }   else if(digit3==010 || digit3==10)   {
                        current_value=2;
                        answer2+=octal_list[current_value];
                    }   else if(digit3==011 || digit3==11)   {
                        current_value=3;
                        answer2+=octal_list[current_value];
                    }    else if(digit3==100)   {
                        current_value=4;
                        answer2+=octal_list[current_value];
                    }    else if(digit3==101)   {
                        current_value=5;
                        answer2+=octal_list[current_value];
                    }    else if(digit3==110)   {
                        current_value=6;
                        answer2+=octal_list[current_value];
                    }   else if(digit3==111)   {
                        current_value=7;
                        answer2+=octal_list[current_value];
                    }
                    numerical/=1000;
                    binary2=Integer.toString(numerical);
                    len_binary=binary2.length();
                }
                digit3=numerical;
                if(digit3==0 || digit3==00) {
                    current_value=0;
                    answer2+=octal_list[current_value];
                }
                else if(digit3==1 || digit3==01) {
                    current_value=1;
                    answer2+=octal_list[current_value];
                }
                else if(digit3==10) {
                    current_value=2;
                    answer2+=octal_list[current_value];
                }
                else if(digit3==11) {
                    current_value=3;
                    answer2+=octal_list[current_value];
                }

                String rev_answer=rev.reverse(answer2);
                System.out.println("The answer is "+rev_answer);
                break;

            case 8:
                System.out.println("You have chosen Binary to HexaDecimal ,Please Enter the Binary Number");
                String hexadecimal2=scan.next();
                len_hexa=hexadecimal2.length();
                try  {
                    for(int counter=0;counter<=len_hexa;counter++)  {
                        char token2=hexadecimal2.charAt(counter);
                        int ascii=token2;
                        while ((ascii>70 && ascii<96) || ascii>102)  {
                            System.out.println("Please Check your Hexadecimal number and try again");
                            hexadecimal2=scan.next();
                            len_hexa=hexadecimal2.length();
                            counter=-1;
                            break;
                        }

                    }

                }  catch (NumberFormatException e)   {

                }
                catch (Exception e)  {

                }

                String[] hexa_list={"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
                int numerical2=Integer.parseInt(hexadecimal2);
                int current_value2=0;
                String answer3="";
                int digit4=0;
                while(hexadecimal2.length()>=4)   {
                    digit4=numerical2%10000;
                    if(digit4==0000 || digit4==0){
                        current_value2=0;
                        answer3+=hexa_list[current_value2];
                    }   else if(digit4==0001 || digit4==1)   {
                        current_value2=1;
                        answer3+=hexa_list[current_value2];
                    }   else if(digit4==0010 || digit4==10)   {
                        current_value2=2;
                        answer3+=hexa_list[current_value2];
                    }   else if(digit4==0011 || digit4==11)   {
                        current_value2=3;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==0100  || digit4==100)   {
                        current_value2=4;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==0101  || digit4==101)   {
                        current_value2=5;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==0110 || digit4==110)   {
                        current_value2=6;
                        answer3+=hexa_list[current_value2];
                    }   else if(digit4==0111 || digit4==111)   {
                        current_value2=7;
                        answer3+=hexa_list[current_value2];
                    }   else if(digit4==1000)   {
                        current_value2=8;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==1001)   {
                        current_value2=9;
                        answer3+=hexa_list[current_value2];
                    }   else if(digit4==1010)   {
                        current_value2=10;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==1011)   {
                        current_value2=11;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==1100)   {
                        current_value2=12;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==1101)   {
                        current_value2=13;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==1110)   {
                        current_value2=14;
                        answer3+=hexa_list[current_value2];
                    }    else if(digit4==1111)   {
                        current_value2=15;
                        answer3+=hexa_list[current_value2];
                    }



                    numerical2/=10000;
                    hexadecimal2=Integer.toString(numerical2);
                    len_hexa=hexadecimal2.length();
                }
                digit4=numerical2;
                if(digit4==0 || digit4==00 || digit4==000) {
                    current_value2=0;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==1 || digit4==01 || digit4==001) {
                    current_value2=1;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==10 || digit4==010) {
                    current_value2=2;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==11 || digit4==011) {
                    current_value2=3;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==100) {
                    current_value2=4;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==101) {
                    current_value2=5;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==110) {
                    current_value2=6;
                    answer3+=hexa_list[current_value2];
                }
                else if(digit4==111) {
                    current_value2=7;
                    answer3+=hexa_list[current_value2];
                }
                String rev_answer2=rev.reverse(answer3);
                System.out.println("The answer is "+rev_answer2);
                break;

            case 9:
                System.out.println("You have chosen Octal to Binary ,Please Enter the Octal Number");
                String octal2=scan.next();
                int leng_octal=octal2.length();
                try {
                    for(int counted=0;counted<=leng_octal;counted++)  {
                        char token=octal2.charAt(counted);
                        int check=Integer.parseInt(String.valueOf(token));
                        while(check<0 || check>7)  {
                            System.out.println("Please Check your Octal number and try again");
                            octal2=scan.next();
                            leng_octal=octal2.length();
                            counted=-1;
                            break;
                        }
                    }
                }   catch (StringIndexOutOfBoundsException e)  {

                }

                String[] bit={"000","001","010","011","100","101","110","111"};
                try     {
                    int start=0;
                    String ob2="";
                    for(int row=0;row<leng_octal;row++) {
                        char och=octal2.charAt(row);
                        start=Integer.parseInt(String.valueOf(och));
                        ob2=bit[start];
                        System.out.print(ob2);
                        start++;
                    }
                }  catch (Exception e) {

                }
                break;

            case 10:
                System.out.println("You have chosen Hexadecimal to Binary ,Please Enter the Hexadecimal Number");
                String hexd=scan.next();
                int len_hexd=hexd.length();
                try  {
                    for(int column=0;column<=len_hexd;column++)  {
                        char token=hexd.charAt(column);
                        int ascii=token;
                        while ((ascii>70 && ascii<96) || ascii>102)  {
                            System.out.println("Please Check your Hexadecimal number and try again");
                            hexd=scan.next();
                            len_hexd=hexd.length();
                            column=-1;
                            break;
                        }

                    }

                }  catch (NumberFormatException e)   {

                }
                catch (Exception e)  {

                }

                String[] hexd_values={"0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"};
                try     {
                    int restart=0;
                    String hb2="";
                    for(int r=0;r<len_hexd;r++) {
                        char ohd=hexd.charAt(r);
                        if(ohd=='A' || ohd=='a')  {
                            restart=10;
                            hb2=hexd_values[restart];
                            System.out.print(hb2);
                        }  else if(ohd=='B' || ohd=='b') {
                            restart=11;
                            hb2=hexd_values[restart];
                            System.out.print(hb2);
                        }  else if(ohd=='C' || ohd=='c') {
                            restart=12;
                            hb2=hexd_values[restart];
                            System.out.print(hb2);
                        }  else if(ohd=='D' || ohd=='d') {
                            restart=13;
                            hb2=hexd_values[restart];
                            System.out.print(hb2);
                        }  else if(ohd=='E' || ohd=='e') {
                            restart=14;
                            hb2=hexd_values[restart];
                            System.out.print(hb2);
                        }  else if(ohd=='F' || ohd=='f') {
                            restart=15;
                            hb2=hexd_values[restart];
                            System.out.print(hb2);
                        }
                        else {
                            restart=Integer.parseInt(String.valueOf(ohd));
                            hb2=hexd_values[restart];
                            System.out.print(hb2);

                        }


                    }
                }  catch (Exception e) {

                }
                break;

            case 11:
                System.out.println("You have chosen Octal to HexaDecimal ,Please Enter the Octal Number");
                String oct=scan.next();
                int len_oct=oct.length();
                try {
                    for(int go=0;go<=len_oct;go++)  {
                        char cur=oct.charAt(go);
                        int cho=Integer.parseInt(String.valueOf(cur));
                        while(cho<0 || cho>7)  {
                            System.out.println("Please Check your Octal number and try again");
                            oct=scan.next();
                            len_oct=oct.length();
                            go=-1;
                            break;
                        }
                    }
                }   catch (StringIndexOutOfBoundsException e)  {

                }
                int ocde2=0;
                int temp_oct=len_oct-1;
                for (int i=0;i<len_oct;i++)  {

                    char cur=oct.charAt(i);
                    int value5=Integer.parseInt(String.valueOf(cur));
                    ocde2+=(value5) * (Math.pow(8,temp_oct));
                    temp_oct--;

                }



                String[] alph={"A","B","C","D","E","F"};
                String dehx="";
                while (ocde2>15) {
                    int remain=ocde2%16;
                    ocde2=ocde2/16;
                    if(remain>9)  {
                        remain-=10;
                        dehx+=alph[remain];
                    }
                    else {
                        dehx+=Integer.toString(remain);
                    }
                    if (ocde2<=15) {
                        if(ocde2>9) {
                            ocde2-=10;
                            dehx+=alph[ocde2];
                        }
                        else {
                            remain=ocde2;
                            dehx+=Integer.toString(remain);
                        }

                    }
                }

                String rev_dehx=rev.reverse(dehx);
                System.out.println("The answer is "+rev_dehx);
                break;

            case 12:
                System.out.println("You have chosen HexaDecimalto Octal ,Please Enter the HexaDecimal Number");
                String hext=scan.next();
                int len_hext=hext.length();
                try  {
                    for(int come=0;come<=len_hext;come++)  {
                        char token_num=hext.charAt(come);
                        int asci=token_num;
                        while ((asci>70 && asci<96) || asci>102)  {
                            System.out.println("Please Check your Hexadecimal number and try again");
                            hext=scan.next();
                            len_hext=hext.length();
                            come=-1;
                            break;
                        }

                    }

                }  catch (NumberFormatException e)   {

                }
                catch (Exception e)  {

                }
                int hede2=0;
                int temp_hext=len_hext-1;
                for (int i=0;i<len_hext;i++)  {
                    int value6=0;
                    char chh=hext.charAt(i);
                    if(chh=='A' || chh=='a')   {
                        value6=10;
                    }
                    else if(chh=='B' || chh=='b')   {
                        value6=11;
                    }
                    else if(chh=='C' || chh=='c')   {
                        value6=12;
                    }
                    else if(chh=='D' || chh=='d')   {
                        value6=13;
                    }
                    else if(chh=='E' || chh=='e')   {
                        value6=14;
                    }
                    else if(chh=='F' || chh=='f')   {
                        value6=15;
                    }
                    else  {
                        value6=Integer.parseInt(String.valueOf(chh));
                    }

                    hede2+=(value6) * (Math.pow(16,temp_hext));
                    temp_hext--;

                }


                String finish ="";
                while (hede2>7) {
                    int m=hede2%8;

                    finish+=Integer.toString(m);
                    hede2=hede2/8;
                    if (hede2<=7) {
                        m=hede2;
                        finish+=Integer.toString(m);
                    }
                }

                String rev_finish=rev.reverse(finish);
                System.out.println("The answer is "+rev_finish);
                break;


        }


    }
}