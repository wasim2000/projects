class ReverseString  {
    public String reverse(String str)  {
        char[] ch=str.toCharArray();
        String value="";
        for(int i=ch.length-1;i>=0;i--)  {
            value+=ch[i];
        }
        return value;
    }
}
